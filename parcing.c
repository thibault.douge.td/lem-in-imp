/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parcing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:07:48 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:59:46 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		find_salle(t_sal *sal, char *str, t_path *path)
{
	int i;

	i = 1;
	while (ft_strcmp(sal->name, str) != 0 && sal->next)
	{
		sal = sal->next;
		i++;
	}
	if (ft_strcmp(sal->name, str) != 0)
		path_with_error(sal, path);
	return (i);
}

void	push_tube(t_sal *sal, char *str, t_path *path)
{
	int		i;
	int		j;
	char	*first;
	char	*second;

	i = 0;
	j = 0;
	while (str[i] != '-' && str[i])
		i++;
	if (str[i] != '-')
		path_with_error(sal, path);
	first = ft_strsub(str, 0, i);
	while (str[i])
	{
		i++;
		j++;
	}
	i = i - (j - 1);
	second = ft_strsub(str, i, j - 1);
	i = find_salle(sal, first, path);
	add_tube(sal, second, i, path);
	i = find_salle(sal, second, path);
	add_tube(sal, first, i, path);
	free(first);
	free(second);
}

void	line_tube(t_sal *sal, int fd, t_path *path)
{
	char *line;

	line = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		if (line[0] != '#')
			push_tube(sal, line, path);
		ft_putstr(line);
		ft_putchar('\n');
		free(line);
		line = NULL;
	}
	if (line)
		free(line);
}

void	nb_ant(int fd, t_path **path)
{
	char *line;

	line = NULL;
	while (get_next_line(fd, &line) && line[0] == '#')
	{
		ft_putstr(line);
		ft_putchar('\n');
		free(line);
		line = NULL;
	}
	error_nb_ant(line);
	(*path) = malloc(sizeof(t_path));
	(*path)->ant = ft_atoi(line);
	(*path)->nxt = NULL;
	ft_putstr(line);
	ft_putchar('\n');
	free(line);
	line = NULL;
}

void	push_salle_in_struct(t_sal *sal, int fd, t_path *path)
{
	char	*line;
	int		ver;

	ver = 0;
	line = NULL;
	while (get_next_line(fd, &line) == 1 && ft_strrchr_lem(line, '-') == NULL)
	{
		if (ft_strcmp(line, "##start") == 0)
			ver = 1;
		if (ft_strcmp(line, "##end") == 0)
			ver = 2;
		else if (line[0] != '#')
			decrypt_line(line, sal, &ver);
		ft_putstr(line);
		ft_putchar('\n');
		free(line);
		line = NULL;
	}
	error_pos(sal);
	free_last(sal);
	init_struct_tube(sal);
	ft_putstr(line);
	ft_putchar('\n');
	push_tube(sal, line, path);
	free(line);
	line_tube(sal, fd, path);
}
