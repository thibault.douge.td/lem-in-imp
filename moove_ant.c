/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moove_ant.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:07:34 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:07:37 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	print_ant(t_path **path)
{
	ft_putchar('L');
	ft_putnbr((*path)->num_ant);
	ft_putchar('-');
	ft_putstr((*path)->salle->name);
}

void	moove_path_2(t_path **path, int *verif, int *j)
{
	while ((*path)->salle->pos != 1)
	{
		if (((*path)->ant == 0 || (*path)->salle->pos == 2) &&
			(*path)->nxt->ant > 0)
		{
			if ((*verif) == 1)
			{
				ft_putchar(' ');
				(*verif) = 0;
			}
			if ((*path)->nxt->salle->pos == 1 && (*path)->nxt->ant > 0 &&
				((*path)->ant == 0 || (*path)->salle->pos == 2))
			{
				(*j)++;
				(*path)->nxt->num_ant = (*j);
			}
			(*path)->nxt->ant--;
			(*path)->num_ant = (*path)->nxt->num_ant;
			(*path)->ant++;
			print_ant(path);
			(*verif) = 1;
		}
		(*path) = (*path)->nxt;
	}
}

void	moove_path(t_path *path)
{
	t_path	*tmp;
	int		i;
	int		j;
	int		verif;

	verif = 0;
	tmp = path;
	i = path->ant;
	path->ant = 0;
	ft_putchar('\n');
	while (path->nxt)
		path = path->nxt;
	path->ant = i;
	path = tmp;
	j = 0;
	while (path->salle->pos != 2 || path->ant != i)
	{
		moove_path_2(&path, &verif, &j);
		verif = 0;
		ft_putchar('\n');
		path = tmp;
	}
}
