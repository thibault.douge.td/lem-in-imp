/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:07:18 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:56:28 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include <stdio.h>

void	print_path(int argc, char **argv, t_path *path)
{
	if (argc == 2)
	{
		if (ft_strcmp(argv[1], "-p") == 0)
		{
			write(1, "\n", 1);
			while (path)
			{
				write(1, "[", 1);
				ft_putstr(path->salle->name);
				write(1, "]", 1);
				if (path->nxt != NULL)
					ft_putstr("<-");
				path = path->nxt;
			}
			write(1, "\n", 1);
		}
	}
}

int		main(int argc, char **argv)
{
	t_sal	*sal;
	t_path	*path;
	t_path	*tmp;

	if (argc > 2)
		return (write(2, "Error param\n", 12));
	if (argc == 2)
		if (ft_strcmp(argv[1], "-p") != 0)
			return (write(2, "Error param\n", 12));
	sal = ft_initialize();
	nb_ant(0, &path);
	push_salle_in_struct(sal, 0, path);
	find_path(sal);
	remp_path(path, sal);
	print_path(argc, argv, path);
	moove_path(path);
	free_sal(sal);
	while (path)
	{
		tmp = path->nxt;
		free(path);
		path = tmp;
	}
	return (0);
}
