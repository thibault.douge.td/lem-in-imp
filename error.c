/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:06:59 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:07:03 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	error_nb_ant(char *line)
{
	int i;

	i = 0;
	if (ft_strlen(line) > 10)
	{
		ft_putstr_fd("Error nb ant\n", 2);
		exit(0);
	}
	if (ft_strlen(line) == 10)
	{
		if (ft_strcmp(line, "2147483647") > 0)
		{
			ft_putstr_fd("Error nb ant\n", 2);
			exit(0);
		}
	}
	while (line[i])
	{
		if (ft_isdigit(line[i]) == 0)
		{
			ft_putstr_fd("Error nb ant\n", 2);
			exit(0);
		}
		i++;
	}
}

void	error_pos(t_sal *sal)
{
	int start;
	int end;

	start = 0;
	end = 0;
	while (sal->next)
	{
		if (sal->pos == 1)
			start++;
		if (sal->pos == 2)
			end++;
		sal = sal->next;
	}
	if (start != 1 || end != 1)
	{
		ft_putstr_fd("Error start or end or '-'\n", 2);
		exit(0);
	}
}

void	error_coord_sal(char *line, int i, int space)
{
	while (line[i])
	{
		if (line[i] == ' ')
		{
			space++;
			i++;
		}
		if (line[i] == '-')
			i++;
		while (ft_isdigit(line[i]) == 1)
			i++;
		if (line[i] != ' ' && line[i] != '\0')
		{
			ft_putstr_fd("Error coord salle\n", 2);
			exit(0);
		}
	}
	if (space != 2)
	{
		ft_putstr_fd("Error space coord salle\n", 2);
		exit(0);
	}
}

void	error_sal(char *line)
{
	int i;
	int space;

	space = 0;
	i = 0;
	while (line[i] != ' ' && line[i])
	{
		if (line[i] == '-')
		{
			ft_putstr_fd("Error name salle '-'\n", 2);
			exit(0);
		}
		i++;
	}
	if (line[i] != ' ')
	{
		ft_putstr_fd("Error coord salle\n", 2);
		exit(0);
	}
	error_coord_sal(line, i, space);
}
