/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:08:11 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:08:12 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	free_last(t_sal *sal)
{
	while (sal->next->next)
		sal = sal->next;
	free(sal->next);
	sal->next = NULL;
}

void	arg_struct(t_sal *sal)
{
	while (sal->next)
		sal = sal->next;
	sal->next = malloc(sizeof(t_sal));
	sal->next->verif = 0;
	sal->next->name = NULL;
	sal->next->next = NULL;
}

t_sal	*ft_initialize(void)
{
	t_sal *sal;

	sal = malloc(sizeof(t_sal));
	sal->name = NULL;
	sal->liais = NULL;
	sal->next = NULL;
	sal->num = 0;
	sal->pos = 0;
	sal->verif = 0;
	return (sal);
}

int		count_maillon(t_sal *sal)
{
	int i;

	i = 0;
	while (sal->next)
	{
		sal = sal->next;
		i++;
	}
	return (i);
}
