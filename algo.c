/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:06:51 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:06:53 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	dead_end(t_sal **sal, t_liais **temp, t_liais **tmp)
{
	(*temp) = (*sal)->liais;
	(*sal)->liais = (*sal)->liais->nxt;
	if (!(*sal)->liais)
	{
		(*sal)->verif = 2;
		(*sal)->liais = (*tmp);
		while ((*sal)->liais->salle->verif != 1 && (*sal)->liais->nxt)
			(*sal)->liais = (*sal)->liais->nxt;
		if ((*sal)->liais->salle->verif != 1 && (*sal)->liais->nxt == NULL)
		{
			ft_putstr_fd("Path invalid\n", 2);
			exit(0);
		}
		(*sal) = (*sal)->liais->salle;
	}
	else
		free(*temp);
}

void	find_path_2(t_sal **sal, t_liais **tmp, t_liais **temp)
{
	(*tmp) = (*sal)->liais;
	if ((*sal)->liais == NULL)
	{
		ft_putstr_fd("Path invalid\n", 2);
		exit(0);
	}
	if ((*sal)->liais->salle->verif == 0)
	{
		(*sal)->liais->salle->verif = 1;
		(*sal) = (*sal)->liais->salle;
	}
	else
		dead_end(sal, temp, tmp);
}

void	find_path(t_sal *sal)
{
	t_liais *tmp;
	t_liais *temp;

	while (sal->pos != 2 && sal)
	{
		sal = sal->next;
	}
	sal->verif = 1;
	while (sal->pos != 1)
	{
		tmp = sal->liais;
		while (tmp)
		{
			if (tmp->salle->pos == 1)
			{
				sal = tmp->salle;
				return ;
			}
			tmp = tmp->nxt;
		}
		find_path_2(&sal, &tmp, &temp);
	}
}

void	remp_path(t_path *path, t_sal *sal)
{
	t_path	*tmp;
	t_liais	*temp;

	tmp = path;
	while (sal->pos != 2)
		sal = sal->next;
	while (sal->pos != 1)
	{
		sal->verif = 2;
		tmp->salle = sal;
		while (sal->liais->salle->verif != 1)
		{
			if (sal->liais->salle->pos == 1)
				break ;
			temp = sal->liais;
			sal->liais = sal->liais->nxt;
			free(temp);
		}
		sal = sal->liais->salle;
		tmp->nxt = malloc(sizeof(t_path));
		tmp = tmp->nxt;
		tmp->ant = 0;
	}
	tmp->salle = sal;
	tmp->nxt = NULL;
}
