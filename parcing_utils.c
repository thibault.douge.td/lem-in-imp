/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parcing_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:07:56 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:07:58 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	decrypt_line(char *line, t_sal *sal, int *ver)
{
	static int num = 1;

	error_sal(line);
	while (sal->next)
		sal = sal->next;
	sal->name = ft_strdup_c(line, ' ');
	sal->num = num;
	sal->pos = *ver;
	num++;
	if (*ver == 1)
		sal->verif = 1;
	*ver = 0;
	arg_struct(sal);
}

void	init_struct_tube(t_sal *sal)
{
	while (sal != NULL)
	{
		sal->liais = NULL;
		sal = sal->next;
	}
}

void	add_tube(t_sal *sal, char *str, int tub, t_path *path)
{
	t_liais	*tmp;
	t_sal	*first;

	first = sal;
	while (sal->next && ft_strcmp(sal->name, str) != 0)
		sal = sal->next;
	if (ft_strcmp(sal->name, str) != 0)
		path_with_error(sal, path);
	if (sal->liais == NULL)
	{
		sal->liais = malloc(sizeof(t_liais));
		tmp = sal->liais;
	}
	else
	{
		tmp = sal->liais;
		while (tmp->nxt != NULL)
			tmp = tmp->nxt;
		tmp->nxt = malloc(sizeof(t_liais));
		tmp = tmp->nxt;
	}
	while (first && first->num != tub)
		first = first->next;
	tmp->salle = first;
	tmp->nxt = NULL;
}
