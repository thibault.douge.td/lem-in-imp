/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:08:05 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:08:06 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

char	*ft_strdup_c(char *s, char c)
{
	int		i;
	char	*str;

	i = 0;
	while (s[i] != c && s[i])
		i++;
	str = (char *)malloc(sizeof(*str) * (i) + 1);
	if (!str)
		return (NULL);
	i = 0;
	while (s[i] != c && s[i])
	{
		str[i] = s[i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

void	path_with_error(t_sal *sal, t_path *path)
{
	find_path(sal);
	remp_path(path, sal);
	moove_path(path);
	exit(0);
}

char	*ft_strrchr_lem(const char *s, int c)
{
	int i;
	int al;

	al = 0;
	i = ft_strlen(s);
	while (al != i)
	{
		if (s[al] == ' ')
			return (NULL);
		if (s[al] == c)
			return ((char *)s + i);
		al++;
	}
	return (NULL);
}
