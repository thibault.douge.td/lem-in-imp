# Lem-in

### Objectifs

Le but du projet est de trouver le moyen le plus rapide de faire traverser la fourmilière par n fourmis. 

Évidemment, il y a quelques contraintes. Ne pas marcher sur ses congénères, tout en évitant les embouteillages, une gestion d'erreur minutieuse permettant de déterminer si un chemin est possible ou pas, détecter une erreur dans les noms des salles.

Au début du jeu, toutes les fourmis sont sur la salle indiquée par la commande (##)start. Le but est de les amener sur la salle indiquée par la commande (##)end en prenant le moins de tours possible. Chaque salle peut contenir une seule fourmi à la fois (sauf (##)start et (##)end qui peuvent en contenir autant qu’il faut).

On considère que les fourmis sont toutes dans la salle (##)start au démarrage.

Vous n’afficherez à chaque tour que les fourmis qui ont bougé.

À chaque tour vous pouvez déplacer chaque fourmi une seule fois et ce suivant un tube (la salle réceptrice doit être libre).

Fonctions autorisées

◦ exit ◦ read ◦ write ◦ malloc ◦ free ◦ perror ◦ strerror

### Objectif personnel

Mon objectif personnel fûs de comprendre les listes chainées car je ne les avais pas utilisées avant.

Réalisation du projet: J'ai mis en place un système de poids pour congestionner les voies sans issues, suite à cela je retiens un chemin et je fais passer mes fourmies les unes aprés les autres.

### Utilisation du programme:

-Faire un Make pour créer l'éxécutable lem-in.

- ./lem-in < nom_de_la_map (lem-in lit sur l'entrée standard), il y a trois maps de tests.

-les coordonnées des salles sont indispensables pour pouvoir passer outre la gestion d'erreur, mais vu que je n'ai pas fais de visualisateur graphique elles n'ont pas d'intéret dans mon projet.

Exemple de map:

3 (Nombre de fourmis)

2 5 0 (2 est le nom de la salle, 5 et 0 sont les coordonnées)

##start

0 1 2 (salle de départ)

##end

1 9 2 (salle d'arrivée)

3 5 4

0-2 (quand il y a un '-' cela définit un tube)

0-3

2-1

3-1

2-3

       [2]
      / | \
    [0] | [1]
      \ | /
       [3]

L: représente le numéro de la fourmie.

-: représente la salle ou se trouve la fourmie.

Un des résultats possibles sur la sortie standard.

L1-3 L2-2

L1-1 L2-1 L3-3

L3-1