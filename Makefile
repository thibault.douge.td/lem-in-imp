# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tdouge <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/01 14:49:40 by tdouge            #+#    #+#              #
#    Updated: 2017/03/01 14:49:43 by tdouge           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem-in

SRC = algo.c \
	error.c \
	free_list.c \
	get_next_line.c \
	lem_in.c \
	moove_ant.c \
	parcing.c \
	parcing_utils.c \
	utils.c \
	utils_list.c

OBJ = $(SRC:.c=.o)

FLAGS	= -Wall -Wextra -Werror

LIB_A = -L./libft/ -lft

.PHONY: all libft clean fclean re

all: $(NAME)

libft:
	make -C libft

$(NAME): $(OBJ) libft
	gcc $(FLAGS) $(OBJ) -o $(NAME) $(LIB_A)

%.o: %.c
	gcc $(FLAGS) -I ./ -I ./libft/ -c $?

clean:
	rm -f $(OBJ)
	make clean -C ./libft/

fclean: clean
	rm -f $(NAME)
	make fclean -C ./libft/

re: fclean all
