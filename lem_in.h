/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:07:26 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:07:27 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# include "libft/libft.h"
# include <fcntl.h>
# define BUFF_SIZE 3

typedef struct		s_path
{
	struct s_sal	*salle;
	struct s_path	*nxt;
	int				num_ant;
	int				ant;
}					t_path;

typedef struct		s_liais
{
	struct s_sal	*salle;
	struct s_liais	*nxt;
}					t_liais;

typedef struct		s_sal
{
	int				nb;
	int				verif;
	int				num;
	char			*name;
	int				pos;
	struct s_sal	*next;
	t_liais			*liais;
}					t_sal;

int					get_next_line(int const fd, char **line);
char				*ft_strdup_c(char *s, char c);
void				free_last(t_sal *sal);
void				arg_struct(t_sal *sal);
t_sal				*ft_initialize();
int					count_maillon(t_sal *sal);
void				add_tube(t_sal *sal, char *str, int tub, t_path *path);
void				init_tab(t_sal *sal, int i);
void				decrypt_line(char *line, t_sal *sal, int *ver);
void				push_salle_in_struct(t_sal *sal, int fd, t_path *path);
void				init_struct_tube(t_sal *sal);
void				find_path(t_sal *sal);
void				remp_path(t_path *path, t_sal *sal);
void				nb_ant(int fd, t_path **path);
void				moove_path(t_path *path);
void				error_nb_ant(char *line);
void				error_pos(t_sal *sal);
void				error_sal(char *line);
void				path_with_error(t_sal *sal, t_path *path);
void				free_sal(t_sal *sal);
char				*ft_strrchr_lem(const char *s, int c);

#endif
