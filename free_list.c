/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdouge <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 17:07:09 by tdouge            #+#    #+#             */
/*   Updated: 2017/02/28 17:07:12 by tdouge           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	free_sal_path(t_sal *sal)
{
	t_liais	*tata;

	while (sal != NULL)
	{
		while (sal->liais != NULL)
		{
			tata = sal->liais->nxt;
			free(sal->liais);
			sal->liais = NULL;
			sal->liais = tata;
		}
		sal = sal->next;
	}
}

void	free_sal(t_sal *sal)
{
	t_sal	*tmp;

	free_sal_path(sal);
	while (sal != NULL)
	{
		tmp = sal;
		sal = sal->next;
		free(tmp->name);
		free(tmp);
	}
}
